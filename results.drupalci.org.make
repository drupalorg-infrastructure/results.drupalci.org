core = 7.x
api = 2
projects[drupal][version] = "7.34"


; Modules
; todo


; Custom modules
; todo


; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"
